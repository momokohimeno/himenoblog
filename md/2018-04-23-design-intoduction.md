---
title:webデザイン＊入門
created_at: 2018-04-23
author: momoko
---



# webデザイン＊入門

#### 【WEB三本柱】

＊HTML 構造（骨組み）

＊CSS スタイル／見た目（文字の色、スタイル）

＊JavaScript 振る舞い（アニメーション、動き）

##### （デザインがシンプルな理由）

写真、キャッチコピー、内容に大きな効果があるから。むしろ写真などに効果がなければマイナスになる。

##### ＜ワンカラム（サイドバーなしの状態＞

注目させたい時はサイドバー無しにする。サイドバーある時は目移りしてもいい時である。

（デザインで使うカラー）メインカラー２つの時は優劣をつける。

＊プライマリーカラー(primary color) => 第一優先

＊セカンダリーカラー(secondary color) => 第二優先



```html

<a class="a" href="/">link</a>

```

### CSS code

```scss

.a {
  color: #000;
  > span {
    font-size: 2rem;
  }
}

```

### JavaScript code

``` js

console.log('popup!!')

(async () => {

  const a = await fetch('test.json');
  console.log(a);

});

```

## Comment

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).
