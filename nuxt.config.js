const webpack = require('webpack');
const path = require('path');

// production or development
const mode = 'development';

module.exports = {

  /*
  ** Headers of the page
  */
  head: {
    title: 'blog',
    titleTemplate: '%s >> hime',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: 'Nuxt.js project'}
    ],
    link: [
      {
        rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
      {
        rel: 'stylesheet',
        href: 'https://cdnjs.cloudflare.com/ajax/libs/bulma/0.4.2/css/bulma.min.css'
      }
    ],
    script: [
    ]
  },
  plugins: [
  ],
  router: {
    // base: mode === 'development' ? '/' : '/himenoblog/',
    base: '/himenoblog/',
  },

  /*
  ** Customize the progress bar color
  */
  loading: {color: '#3B8070'},

  module: [
    // or array of paths
    ['nuxt-sass-resources-loader', [
      '@/assets/sass/foundation/variable.scss',
      '@/assets/sass/foundation/mixin.scss',
    ]],
  ],

  /*
  ** Build configuration
  */
  build: {

    /*
    ** Run ESLint on save
    */
    extend(config, ctx) {

      if (ctx.isDev && ctx.isClient) {

        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })

      }

      /**
       * import 用の alias
       */
      config.resolve.alias['~'] =  path.resolve(__dirname);
      config.resolve.alias['@'] =  path.resolve(__dirname);

    },
    // プラグインの重複を防ぐ
    // vendor: ['axios'],
    plugins: [
      new webpack.ProvidePlugin({
        // '_': 'lodash'
      }),
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || mode),
      }),
    ]
  },
  generate: {
    // 動的なパラメーターを用いたルートを生成させたい場合
    // 動的なルーティングの配列をセットする
    routes(callback) {

      // const posts = require('./assets/json/posts.json');
      // post/idの配列を生成
      // let routes = posts.map(post => `/post/${post.id}`);
      // callback(null, routes)

      const {sourceFileArray} = require('./json/summary.json');
      const urlSet = str => str.replace(/md\//, 'post/').replace(/(\d{4}-\d{2}-\d{2})-/, '$1/').replace(/.md/, '');

      const routes = sourceFileArray.map(sourceFileName => {
        return urlSet(sourceFileName);
      });
      callback(null, routes);

    }
  }
};
